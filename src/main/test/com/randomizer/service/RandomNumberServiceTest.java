package main.test.com.randomizer.service;

import main.java.com.randomizer.service.RandomNumberService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Random;
import java.util.Scanner;

public class RandomNumberServiceTest {
    private final Scanner scanner = Mockito.mock(Scanner.class);
    private final Random random = Mockito.mock(Random.class);
    private final RandomNumberService cut = new RandomNumberService(scanner);


    static Arguments[] continueOrExitTestArgs() {
        return new Arguments[]{
                Arguments.arguments("r", true),
                Arguments.arguments("n", false)
        };
    }

    @ParameterizedTest
    @MethodSource("continueOrExitTestArgs")
    void continueOrExitTest(String expScanner, boolean expected) {
        Mockito.when(scanner.nextLine()).thenReturn(expScanner);

        boolean actual = cut.continueOrExit();

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] scanResponseTestArgs() {
        return new Arguments[]{
                Arguments.arguments("check", "check"),
                Arguments.arguments("hello", "hello")
        };
    }

    @ParameterizedTest
    @MethodSource("scanResponseTestArgs")
    void scanResponseTest(String expScanner, String expected) {
        Mockito.when(scanner.nextLine()).thenReturn(expScanner);

        String actual = cut.scanResponse();

        Assertions.assertEquals(expected, actual);
    }


//    static Arguments[] generateRandomNumberTestArgs() {
//        return new Arguments[]{
//                Arguments.arguments("r", true),
//                Arguments.arguments("n", false)
//        };
//    }
//
//    @ParameterizedTest
//    @MethodSource("generateRandomNumberTestArgs")
//    void generateRandomNumberTest(String expScanner, boolean expected) {
//        Mockito.when(scanner.nextLine()).thenReturn(expScanner);
//
//        boolean actual = cut.generateRandomNumber();
//
//        Assertions.assertEquals(expected, actual);
//    }
}
