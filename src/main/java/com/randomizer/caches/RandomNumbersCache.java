package main.java.com.randomizer.caches;

import java.util.ArrayList;
import java.util.List;

public class RandomNumbersCache {

    private List<Integer> randomNumbers;

    public RandomNumbersCache(){
        this.randomNumbers = new ArrayList<>();
    }

    public void addRepeatableNumberCache(int repeatableNumb){
        randomNumbers.add(repeatableNumb);
    }

    public boolean checkRepeatableNumAtCache(int repeatableNum){
        return randomNumbers.contains(repeatableNum);
    }

    public int getSize(){
        return randomNumbers.size();
    }

    public void clearRepeatableNumbers(){
        randomNumbers.clear();
    }
}
