package main.java.com.randomizer.service;

import main.java.com.randomizer.caches.RandomNumbersCache;

import java.util.Random;
import java.util.Scanner;

public class RandomNumberService {
    private static final String MSG_WELCOME = "Welcome to the Randomizer!";
    private static final String MSG_CHOOSE_RANGE = "Enter the range (minimum - 0, maximum - 500)";
    private static final String MSG_AVAILABLE_COMMAND = "Enter the available command!";
    private static final String MSG_INVALID_MIN = "Invalid number! The number can't be less than 1 and higher or equal to 500!";
    private static final String MSG_INVALID_MAX = "Invalid number! The number can't be higher than 500 and lower or equal to minimum!";
    private static final String MSG_MIN = "minimum: ";
    private static final String MSG_MAX = "maximum: ";
    private static final String MSG_CONTINUE_OR_EXIT = "Enter any symbol to continue or n to exit...";
    private static final String MSG_REPEAT_OR_NOT = "Numbers is ended!\nEnter r to repeat or any symbol to exit...";
    private static final String MSG_APP_IS_CLOSED = "Application inactive anymore. Bye!";
    private static final String MSG_CHOOSE_COMMAND = "Enter generate, help or exit...";
    private static final String RESPONSE_GENERATE = "generate";
    private static final String RESPONSE_HELP = "help";
    private static final String RESPONSE_EXIT = "exit";
    private static final String RESPONSE_N = "n";
    private static final String RESPONSE_R = "r";

    private Scanner scanner;
    private Random random;

    RandomNumbersCache randomNumbersCache = new RandomNumbersCache();

    public static int min, max;

    public RandomNumberService(Scanner scanner) {
        this.scanner = scanner;
    }

    public void menu(){
        System.out.println(MSG_WELCOME);
        while(true) {
            System.out.println(MSG_CHOOSE_COMMAND);
            startApp(scanResponse());

        }
    }

    public boolean continueOrExit(){
        System.out.println(MSG_CONTINUE_OR_EXIT);
        String answer = scanResponse();
        if(answer.equalsIgnoreCase(RESPONSE_N)) {
            return false;
        } else {
            return true;
        }
    }

    public void generateRandomNumber(){
        chooseRange();

        int diff = max - min;
        random = new Random();

        while(true) {
            if ((diff + 1) == randomNumbersCache.getSize()) {
                System.out.println(MSG_REPEAT_OR_NOT);
                if(scanResponse().equalsIgnoreCase(RESPONSE_R)) {
                    randomNumbersCache.clearRepeatableNumbers();
                } else
                    break;
            }
            int randNum = random.nextInt(diff + 1);

            randNum += min;
            if (randomNumbersCache.checkRepeatableNumAtCache(randNum))
                continue;
            randomNumbersCache.addRepeatableNumberCache(randNum);
            System.out.println(randNum);
            if(!continueOrExit()){
                break;
            }
        }
    }

    public void chooseRange(){
        System.out.println(MSG_CHOOSE_RANGE);
        while(true) {
            System.out.print(MSG_MIN);
            min = Integer.parseInt(scanner.nextLine());
            System.out.println();
            if(min<1 || min >= 500){
                System.out.println(MSG_INVALID_MIN);
            } else
                break;
        }
        while(true) {
            System.out.println(MSG_MAX);
            max = Integer.parseInt(scanner.nextLine());
            System.out.println();
            if(max>500 || max<=min){
                System.out.println(MSG_INVALID_MAX);
            } else
                break;
        }
    }

    public String scanResponse(){
        return scanner.nextLine();
    }

    public void helper(){
        System.out.println("----------------------------------------------------------------------------------------");
        System.out.println("Description: ");
        System.out.println("The task of the application is to generate a random number from a user-specified range.");
        System.out.println("Commands and their description: ");
        System.out.println("generate - generate a random number from range;");
        System.out.println("help - displays the description and commands of the application;");
        System.out.println("exit - closes the program.");
        System.out.println("----------------------------------------------------------------------------------------");
    }

    private void startApp(String response){
        if(response.equalsIgnoreCase(RESPONSE_GENERATE)){
            generateRandomNumber();
        }else if(response.equalsIgnoreCase(RESPONSE_HELP)){
            helper();
        }else if(response.equalsIgnoreCase(RESPONSE_EXIT)){
            System.out.println(MSG_APP_IS_CLOSED);
            System.exit(1);
        }else
            System.out.println(MSG_AVAILABLE_COMMAND);
    }
}
