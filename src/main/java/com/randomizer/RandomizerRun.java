package main.java.com.randomizer;

import main.java.com.randomizer.caches.RandomNumbersCache;
import main.java.com.randomizer.service.RandomNumberService;

import java.util.Scanner;

public class RandomizerRun {
    public static void main(String [] args){

        Scanner scanner = new Scanner(System.in);

        RandomNumbersCache randomNumbersCache = new RandomNumbersCache();

        new RandomNumberService(scanner).menu();
    }
}
